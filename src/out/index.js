import console from 'console';

import chalk from 'chalk';

let isInDebugMode = false;

const init = (debugMode) => {
    isInDebugMode = debugMode;
};

const log = (prefix, color, txt) => {
    txt.map((entry) => console.log(color(`${prefix} ${entry}`)));
};

const info = (...txt) => {
    log('[*]', chalk.green, txt);
};

const warn = (...txt) => {
    log('[-]', chalk.yellowBright, txt);
};

const debug = (...txt) => {
    if (isInDebugMode) log('[d]', chalk.white, txt);
};

const error = (...txt) => {
    log('[!]', chalk.red, txt);
};

export default {
    info,
    init,
    warn,
    error,
    debug,
};
